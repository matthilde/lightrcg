/*
 * Raycaster by matthilde
 *
 * raycaster.c
 *
 * The raycasting engine itself.
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <lightrcg/lightrcg.h>

RayMap* R_NewMap(int w, int h) {
    RayTile* buf = (RayTile*)xmalloc(sizeof(RayTile) * w * h);
    RayTile** ptrs = (RayTile**)xmalloc(sizeof(RayTile*) * w);
    RayFC* fcbuf = (RayFC*)xmalloc(sizeof(RayFC) * w * h);
    RayFC** fc_ptrs = (RayFC**)xmalloc(sizeof(RayFC*) * w);
    Color* lmbuf = (Color*)xmalloc(sizeof(Color) * w * h);
    Color** lm_ptrs = (Color**)xmalloc(sizeof(Color*) * w);
    for (int i = 0; i < w; ++i) {
        ptrs[i] = (buf + (i*h));
        fc_ptrs[i] = (fcbuf + (i*h));
        lm_ptrs[i] = (lmbuf + (i*h));
    }
    for (int i = 0; i < w * h; ++i) {
        buf[i] = (RayTile){0, 0};
        fcbuf[i] = (RayFC){0, 0};
        lmbuf[i] = RGBA(0, 0, 0, 255);
    }

    RayMap *map = STRUCTALLOC(RayMap);
    map->width = w; map->height = h;
    map->buffer = ptrs; map->floormap = fc_ptrs;
    map->lightmap = lm_ptrs;
    return map;
}
// returns NULL if something has failed.
RayMap* R_LoadSimpleMap(const char* filename) {
    DEBUGLOG("Loading simple map %s...", filename);
    FILE *f = fopen(filename, "r");
    if (!f) {
        DEBUGLOG("Loading simple map file has failed.");
        return NULL;
    }

    int w, h, c;
    fscanf(f, "%d %d", &w, &h);
    // our map
    RayMap *m = R_NewMap(w, h);
    RayTile *buf = m->buffer[0];
    // now time to get the map array
    for (size_t i = 0; i < (size_t)(w * h);)
        switch (c = fgetc(f)) {
        case EOF:
            DEBUGLOG("Loading of map failed: unexpected EOF.");
            return NULL;
            break;
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            buf[i++].b = c - '0';
            buf[i].flags = buf[i].b > 0 ? R_BLOCKFLAG_COLLIDE : 0;
            break;
        default:
            break;
        }
    return m;
}
void R_FreeMap(RayMap* m) {
    free(m->buffer[0]); free(m->buffer);
    free(m->floormap[0]); free(m->floormap);
    free(m->lightmap[0]); free(m->lightmap);
    free(m);
}

void R_ClearFramebuffer(RayBuffer* fb, Color color) {
    for (int i = 0; i < (fb->width * fb->height); ++i)
        fb->pixels[0][i] = color;
}

RayBuffer* R_NewFramebuffer(int w, int h) {
    // c pointer stinky hacks
    // allows for compatibility with making SDL textures for woowee
    // faster rendering
    // also O(1) free-ing
    Color* pixmap = (Color*)xmalloc(sizeof(Color) * w * h);
    Color** pixels = (Color**)xmalloc(sizeof(Color*) * h);
    for (int i = 0; i < h; ++i)
        pixels[i] = (pixmap + (i*w));

    RayBuffer *buffer = STRUCTALLOC(RayBuffer);
    buffer->width = w; buffer->height = h;
    buffer->pixels = pixels;

    return buffer;
}
void R_FreeFramebuffer(RayBuffer* fb) {
    free(fb->pixels[0]); free(fb->pixels); free(fb);
}

RaySpriteBuffer* R_NewSpriteBuffer(int size) {
    RaySpriteBuffer *sb = STRUCTALLOC(RaySpriteBuffer);
    sb->length = size;
    sb->used = 0;
    sb->sprites = (RaySprite*)xmalloc(sizeof(RaySprite) * size);
    sb->info = (struct _rsprite_distinfo*)xmalloc(
        sizeof(struct _rsprite_distinfo) * size);

    return sb;
}
void R_FreeSpriteBuffer(RaySpriteBuffer *sb) {
    free(sb->sprites);
    free(sb->info);
    free(sb);
}
// Returns -1 if the sprite buffer is full
int R_PushSprite(RaySpriteBuffer *sb, RayTexture* tex,
                 Vector p, byte flags) {
    int id = sb->used;
    if (id == sb->length) return -1;
    RaySprite* s = (sb->sprites + id);
    s->t.texture = tex;
    s->visible = 1;
    s->pos = p;
    s->zHeight = 0;
    s->flags = flags;
    s->isDirectional = 0;

    sb->used++;
    return id;
}
// directional sprites woooooow
int R_PushDirSprite(RaySpriteBuffer *sb, RayTexture **tex,
                    int directions, Vector p, byte flags) {
    int id = R_PushSprite(sb, NULL, p, flags);
    if (id == -1) return -1;

    RaySprite *s = (sb->sprites + id);
    s->t.textures = tex;
    s->angle = 0;
    s->directions = directions;
    s->isDirectional = 1;

    return id;
}
// animated sprites :eyes:
void R_UpdateSpritesFrame(Raycaster* r, RaySpriteBuffer* sb) {
    RaySprite *s = sb->sprites;
    for (int i = 0; i < sb->used; ++i) {
        if (s->visible) {
            if (s->isDirectional)
                for (int dir = 0; dir < s->directions; ++dir)
                    R_UpdateTextureFrame(r, s->t.textures[dir]);
            else
                R_UpdateTextureFrame(r, s->t.texture);
        }
        s++;
    }
}

// Allocates raycast info for R_CalculateWalls
RaycastInfo *R_AllocateRayInfo(int size) {
    // funny allocation method 2: electric boogaloo
    RaycastInfo* ri = STRUCTALLOC(RaycastInfo);
    RayRay *raybuffer = (RayRay*)xmalloc(sizeof(RayRay) * size * MAX_Z);
    RayRay **rays = (RayRay**)xmalloc(sizeof(RayRay*) * size);
    for (int i = 0; i < size; ++i)
        rays[i] = (raybuffer + (i*MAX_Z));

    ri->size = size; ri->depth = MAX_Z;
    ri->rays = rays;

    // Fog and pitch stuff will be added later.
    return ri;
}
void R_FreeRayInfo(RaycastInfo* ri) {
    free(ri->rays[0]);
    free(ri->rays);
    free(ri);
}

// Creates a new raycaster with pre-allocated framebuffer
// Arguments are the width and height of the framebuffer.
Raycaster* R_NewRaycaster(int width, int height) {
    Raycaster* r = STRUCTALLOC(Raycaster);
    r->player.x = r->player.y = 3.5;
    r->direction.x = -1; r->direction.y = 0; r->angle = 270;
    r->plane.x = 0; r->plane.y = 0.66;
    r->fogEnabled = 0;
    // 90 degrees angle for faster rendering (i hope)
    r->buffer = R_NewFramebuffer(width, height);
    r->textures = NULL; r->map = NULL;
    r->zbuffer = R_AllocateRayInfo(width);

    return r;
}

// NOTE: Textures needs to be de-allocated seperately as the
//       Raycaster struct does not contain the size of the bank.
void R_FreeRaycaster(Raycaster* r) {
    if (r->buffer) R_FreeFramebuffer(r->buffer);
    if (r->zbuffer) R_FreeRayInfo(r->zbuffer);
    if (r->map) R_FreeMap(r->map);
    free(r);
}

static double ray_subgrid_calculate(Vector p, Vector d, int *side) {
    Vector dDist, sDist;
    Point  mapPos, step;
    int hit;

    byte map[5][5] = {
        { 1, 1, 1, 1, 0 },
        { 1, 0, 0, 0, 0 },
        { 1, 0, 0, 0, 0 },
        { 1, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0 },
    };

    dDist.x = (d.x == 0) ? 1e30 : fabs(0.25 / d.x);
    dDist.y = (d.y == 0) ? 1e30 : fabs(0.25 / d.y);
    mapPos.x = (int)p.x; mapPos.y = (int)p.y;

    hit = 0;
    if (d.x < 0) {
        step.x = -1;
        sDist.x = (p.x - mapPos.x) * dDist.x;
    } else {
        step.x = 1;
        sDist.x = (mapPos.x + 1.0 - p.x) * dDist.x;
    }
    if (d.y < 0) {
        step.y = -1;
        sDist.y = (p.y - mapPos.y) * dDist.y;
    } else {
        step.y = 1;
        sDist.y = (mapPos.y + 1.0 - p.y) * dDist.y;
    }

    while (!hit) {
        if (sDist.x < sDist.y) {
            sDist.x += dDist.x;
            mapPos.x += step.x;
            *side = 0;
        }
        else {
            sDist.y += dDist.y;
            mapPos.y += step.y;
            *side = 1;
        }

        if (map[mapPos.x][mapPos.y]) hit = 1;
        if (mapPos.x < 0 || mapPos.x >= 4 ||
            mapPos.y < 0 || mapPos.y >= 4)
            return -1.0;
    }

    double wallDist;
    if (*side == 0) wallDist = sDist.x;
    else            wallDist = sDist.y;

    return wallDist;
}

// Renders the stuff
void R_CalculateWalls(Raycaster *r) {
    RaycastInfo *ri = r->zbuffer;
    RayMap  *map = r->map;
    // "unpacking" everything for faster pointer access
    Vector p = r->player, d = r->direction;
    RayTexture **textures = r->textures;
    if (map == NULL) return; // TODO: Error

    // Doing all the variable stuff
    int w = r->buffer->width; // int h = r->buffer->height;

    int hit, side;
    double cameraX, wallDist;
    Vector rayDir, dDist, sDist;
    Point mapPos, step;
    RayRay **rays = ri->rays, *ray;
    RayTile curTile;
    
    // Let's goooooooo
    for (int x = 0; x < ri->size; ++x) {
        ray = rays[x];
        cameraX = 2 * x / (double)w - 1;
        rayDir.x = d.x + r->plane.x * cameraX;
        rayDir.y = d.y + r->plane.y * cameraX;

        mapPos.x = (int)p.x; mapPos.y = (int)p.y;

        dDist.x = (rayDir.x == 0) ? 1e30 : fabs(1.0 / rayDir.x);
        dDist.y = (rayDir.y == 0) ? 1e30 : fabs(1.0 / rayDir.y);

        hit = 0;
        if (rayDir.x < 0) {
            step.x = -1;
            sDist.x = (p.x - mapPos.x) * dDist.x;
        } else {
            step.x = 1;
            sDist.x = (mapPos.x + 1.0 - p.x) * dDist.x;
        }
        if (rayDir.y < 0) {
            step.y = -1;
            sDist.y = (p.y - mapPos.y) * dDist.y;
        } else {
            step.y = 1;
            sDist.y = (mapPos.y + 1.0 - p.y) * dDist.y;
        }

        // dda moment
        // This could be put in a loop until the map length is reached
        // This could also allow to avoid segfaulting when the ray
        // goes outside of boundaries...
        int z = 0; double wallOffset; Vector tmpDist;
        for (z = 0; z < ri->depth; ++z) {
            hit = 0;
            // TODO: Finish writing this and add allocator
            while (hit == 0) {
                if (sDist.x < sDist.y) {
                    sDist.x += dDist.x;
                    mapPos.x += step.x;
                    side = 0;
                }
                else {
                    sDist.y += dDist.y;
                    mapPos.y += step.y;
                    side = 1;
                }

                if (mapPos.x < 0 || mapPos.x >= map->width ||
                    mapPos.y < 0 || mapPos.y >= map->height)
                    goto _raycaster_end_z_buffering;
                // if (map->buffer[mapPos.x][mapPos.y].b) hit = 1;
                curTile = map->buffer[mapPos.x][mapPos.y];

                if (curTile.b) {
                    if (curTile.flags & R_BLOCKFLAG_THIN) {
                        // Once this prototype is done, it would be
                        // better if it gets refactored...
                        if (side == 0)
                            wallOffset = p.y + (sDist.x - dDist.x) * rayDir.y;
                        else
                            wallOffset = p.x + (sDist.y - dDist.y) * rayDir.x;
                        wallOffset -= floor(wallOffset);
                        wallOffset *= 4;

                        if (side == 0) {
                            tmpDist.y = wallOffset;
                            tmpDist.x = side == 1 ? 0 : 4;
                        } else {
                            tmpDist.x = wallOffset;
                            tmpDist.y = side == 1 ? 0 : 4;
                        }

                        double mu = ray_subgrid_calculate(tmpDist,
                                                          rayDir,
                                                          &side);

                        if (mu < 0)
                            hit = 0;
                        else if (side == 0) {
                            sDist.x += mu;
                            hit = 1;
                        } else {
                            sDist.y += mu;
                            hit = 1;
                        }
                        
                    } else
                        hit = 1;
                }
            }

            if (side == 0) wallDist = sDist.x - dDist.x;
            else           wallDist = sDist.y - dDist.y;


            if (side == 0) {
                ray[z].dir = step.x == 1 ? R_DIR_LEFT : R_DIR_RIGHT;
            } else {
                ray[z].dir = step.y == 1 ? R_DIR_UP   : R_DIR_DOWN;
            }

            ray[z].pos = mapPos;
            ray[z].texture = R_GetTextureBuffer(
                textures[map->buffer[mapPos.x][mapPos.y].b - 1]);
            ray[z].side    = side;
            ray[z].dist    = wallDist;
            ray[z].rayDir.x = rayDir.x;
            ray[z].rayDir.y = rayDir.y;
        }
    _raycaster_end_z_buffering:
        for (; z < ri->depth; ++z) {
            ray[z].texture = NULL;
        }
    }
}
