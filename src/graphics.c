/*
 * raycasting engine by matthilde
 *
 * graphics.c
 *
 * Does anything rendering and graphics related. SDL front-end will be
 * put somewhere so it can be ported to another graphics library
 * if required.
 */
#include <stdlib.h>

#include <lightrcg/lightrcg.h>

// Fog calculation algorithms
static float r_fog_multiplier(double wallDist,
                              float fogDist,
                              float fogMul) {
    if (wallDist > fogDist) {
        float fogOpacity = 1.0 - ((wallDist - fogDist) * fogMul);
        if (fogOpacity > 0.0) {
            return fogOpacity;
        } else
            return 0.0;
    }
    return 1.0;
}

static inline void r_color_mul(Color* c, float mul) {
    c->r *= mul; c->g *= mul; c->b *= mul;
}
static void r_fog_apply(Color* c, Color fogColor, float opacity) {
    if (opacity == 0.0) {
        *c = fogColor;
    } else if (opacity != 1.0) {
        float revOpacity = 1.0 - opacity;
        c->r = (c->r * opacity) + (fogColor.r * revOpacity);
        c->g = (c->g * opacity) + (fogColor.g * revOpacity);
        c->b = (c->b * opacity) + (fogColor.b * revOpacity);
    }
}

// NOTE: The RaycastInfo size should match the resolution.
void R_RenderRaycastInfo(Raycaster *r) {
    RaycastInfo *ri = r->zbuffer;
    Color **lightmap = r->map->lightmap;
    int lineHeight, start, end, side, texWidth, texHeight;
    int pitch = 0, texX, texY;
    RayBuffer* texture; RayRay *ray;
    double wallDist, stepT, texPos;
    Point tilePos;
    Vector wallOff;
    Color color;
    float blockLuma;

    int fogEnabled = r->fogEnabled;
    float fogDistance = r->fogDistance, fogIntensity = r->fogIntensity;
    Color fogColor = r->fogColor;
    float fogCache = 1.0;

    int h = r->buffer->height, w = r->buffer->width;
    int mh = r->map->height, mw = r->map->width;
    Vector rayDir, p = r->player;
    RayBuffer* fb = r->buffer;
    
    for (int x = 0; x < ri->size; ++x) {
        for (int z = ri->depth - 1; z >= 0; --z) {
            ray = (ri->rays[x] + z);
            wallDist = ray->dist;
            texture = ray->texture;
            tilePos = ray->pos;
            side = ray->side;

            // hit detect wall
            if (x == w / 2) {
                if (texture) {
                    r->hit.type = R_HIT_BLOCK;
                    r->hit.info.block = tilePos;
                } else
                    r->hit.type = R_HIT_NONE;
            }
            if (texture == NULL) continue;

            rayDir = ray->rayDir;
            
            lineHeight = (int)(h / wallDist);
            pitch = 0;

            switch (ray->dir) {
            case R_DIR_RIGHT: tilePos.x++; break;
            case R_DIR_LEFT:  tilePos.x--; break;
            case R_DIR_UP:    tilePos.y--; break;
            case R_DIR_DOWN:  tilePos.y++; break;
            }
            if (tilePos.x < 0 || tilePos.y < 0 ||
                tilePos.x >= mw || tilePos.y >= mh)
                ;
            else
                blockLuma = (float)lightmap[tilePos.x][tilePos.y].a / 256;

            start = -lineHeight / 2 + h / 2 + pitch;
            if (start < 0) start = 0;
            end   = lineHeight / 2 + h / 2 + pitch;
            if (end >= h)  end = h - 1;

            texWidth = texture->width; texHeight = texture->height;

            if (side == 0) wallOff.x = p.y + wallDist * rayDir.y;
            else           wallOff.x = p.x + wallDist * rayDir.x;
            wallOff.x -= floor(wallOff.x);

            texX = (int)(wallOff.x * (double)texWidth);
            if (side == 0 && rayDir.x > 0) texX = texWidth - texX - 1;
            if (side == 1 && rayDir.y < 0) texX = texWidth - texX - 1;

            stepT = 1.0 * texHeight / lineHeight;
            texPos = (start - pitch - h / 2 + lineHeight / 2) * stepT;

            if (fogEnabled)
                fogCache = r_fog_multiplier(wallDist,
                                            fogDistance,
                                            fogIntensity);

            for (int y = start; y < end; ++y) {
                texY = pmod((int)texPos, texHeight); // (texHeight - 1);
                texPos += stepT;
                color = texture->pixels[texY][texX];
                if (color.a > 0) {
                    r_color_mul(&color, blockLuma);
                    /*
                    if (side == 1) {
                        color.r *= 0.5;
                        color.g *= 0.5;
                        color.b *= 0.5;
                        } */
                    if (fogEnabled)
                        r_fog_apply(&color, fogColor, fogCache);
                    fb->pixels[y][x] = color;
                }
            }

        }
    }
}

// Renders the floor and the ceiling.
// TODO: Implement floor and ceiling map
void R_RenderPlate(Raycaster* r) {
    // wooooo
    int screenWidth  = r->buffer->width;
    int screenHeight = r->buffer->height;
    Vector d = r->direction, plane = r->plane, pl = r->player;
    RayFC **floormap = r->map->floormap;
    Color **lightmap = r->map->lightmap;
    int mw = r->map->width, mh = r->map->height;
    RayTexture** textures = r->textures;

    int p;
    int tw, th;
    Color color;
    double posZ, rowDistance;
    Point cell, texPos;
    Vector rayDir0, rayDir1, floorStep, floor;
    float fogCache = 1.0;
    float fogDistance = r->fogDistance, fogIntensity = r->fogIntensity;
    Color fogColor = r->fogColor;
    int fogEnabled = r->fogEnabled;

    RayFC cellID, ocellID = (RayFC){ -1, -1 };
    RayBuffer *floorTex = NULL, *ceilTex = NULL;
    float blockLuma;

    for (int y = screenHeight / 2; y < screenHeight; ++y) {
        rayDir0.x = d.x - plane.x;
        rayDir0.y = d.y - plane.y;
        rayDir1.x = d.x + plane.x;
        rayDir1.y = d.y + plane.y;

        p = y - screenHeight / 2;
        posZ = 0.5 * screenHeight;

        rowDistance = posZ / p;
        floorStep.x = rowDistance * (rayDir1.x - rayDir0.x) / screenWidth;
        floorStep.y = rowDistance * (rayDir1.y - rayDir0.y) / screenWidth;

        floor.x = pl.x + rowDistance * rayDir0.x;
        floor.y = pl.y + rowDistance * rayDir0.y;

        if (fogEnabled)
            fogCache = r_fog_multiplier(rowDistance, fogDistance, fogIntensity);

        for (int x = 0; x < screenWidth; ++x) {
            cell.x = (int)floor.x;
            cell.y = (int)floor.y;
            /*
            if (cell.x < 0 || cell.x >= mw ||
                cell.y < 0 || cell.y >= mh)
                continue;
            */
            if ((cell.x < 0 || cell.x >= mw) ||
                (cell.y < 0 || cell.y >= mh)) {
                floor.x += floorStep.x; floor.y += floorStep.y;
                continue;
            }
            cellID = floormap[cell.x][cell.y];
            blockLuma = (float)lightmap[cell.x][cell.y].a / 255;

            if (cellID.floor) {
                if (cellID.floor != ocellID.floor)
                    floorTex = R_GetTextureBuffer(textures[cellID.floor - 1]);
                tw = floorTex->width; th = floorTex->height;
                texPos.x = pmod((int)(tw * (floor.x - cell.x)), tw);
                texPos.y = pmod((int)(th * (floor.y - cell.y)), th);
                
                color = floorTex->pixels[th - 1 - texPos.y][texPos.x];
                if (color.a) {
                    r_color_mul(&color, blockLuma);
                    if (fogEnabled)
                        r_fog_apply(&color, fogColor, fogCache);

                    r->buffer->pixels[y][x] = color;
                }
            }
            if (cellID.ceiling) {
                if (cellID.ceiling != ocellID.ceiling)
                    ceilTex = R_GetTextureBuffer(textures[cellID.ceiling - 1]);
                tw = ceilTex->width; th = ceilTex->height;
                texPos.x = pmod((int)(tw * (floor.x - cell.x)), tw);
                texPos.y = pmod((int)(th * (floor.y - cell.y)), th);

                color = ceilTex->pixels[texPos.y][texPos.x];
                if (color.a) {
                    r_color_mul(&color, blockLuma);
                    if (fogEnabled)
                        r_fog_apply(&color, fogColor, fogCache);

                    r->buffer->pixels[screenHeight - y][x] = color;
                }
            }
            ocellID = cellID;
            floor.x += floorStep.x; floor.y += floorStep.y;
        }
    }
}

// I recommend to run this right after calculating and rendering
// walls.
void R_RenderSprites(Raycaster *r, RaySpriteBuffer *sb) {
    if (sb->used == 0) return; // i mean, there is nothing to render
    struct _rsprite_distinfo *sdi = sb->info;
    RaycastInfo *ri = r->zbuffer; RayRay *currentRay;
    RaySprite* sprites = sb->sprites, *curSprite;
    RayBuffer* tex;
    Color **buffer = r->buffer->pixels;
    Color **lightmap = r->map->lightmap;
    Vector p = r->player, plane = r->plane, delta;
    Vector d = r->direction;
    Vector spritePos, transform;
    Point drawStart, drawEnd, texPos;
    int spriteWidth, spriteHeight, spriteScreenX, tmp, dsxold;
    double invDet;
    Color color;
    float fogCache = 1.0;
    float fogDistance = r->fogDistance, fogIntensity = r->fogIntensity;
    Color fogColor = r->fogColor;
    int fogEnabled = r->fogEnabled;
    float blockLuma;

    int w = r->buffer->width, h = r->buffer->height;
    int mw = r->map->width, mh = r->map->height;
    Point sprpos; int zHeight;

    // sort sprites by distance
    for (int i = 0; i < sb->used; ++i) {
        sdi[i].order = i;
        delta.x = p.x - sprites[i].pos.x;
        delta.y = p.y - sprites[i].pos.y;
        sdi[i].distance = delta.x * delta.x + delta.y * delta.y;
    }
    R_SortSpriteOrder(sb);

    for (int i = sb->used - 1; i >= 0; --i) {
        curSprite = (sprites + sdi[i].order);
        if (!curSprite->visible) continue;
        if (curSprite->isDirectional) {
            // int deltaAngle = pmod((int)(curSprite->angle - playerAngle + 90.0), 360); // % 360;
            // deltaAngle = (deltaAngle * 8) / 360;
            // printf("%d\n", deltaAngle);

            Vector delta;
            delta.x = p.x - curSprite->pos.x;
            delta.y = p.y - curSprite->pos.y;

            int deltaAngle = (int)(atan2(delta.x, delta.y) * (180/PI)) + curSprite->angle - 90;
            // deltaAngle -= 360 / (curSprite->directions * 2);
            deltaAngle = (deltaAngle * curSprite->directions) / 360;
            
            deltaAngle = pmod(deltaAngle, 8);

            tex = R_GetTextureBuffer(curSprite->t.textures[deltaAngle]);
        } else
            tex = R_GetTextureBuffer(curSprite->t.texture);

        sprpos.x = curSprite->pos.x; sprpos.y = curSprite->pos.y;
        if (sprpos.x < 0 || sprpos.y < 0 ||
            sprpos.x >= mw || sprpos.y >= mh)
            blockLuma = 0.0;
        else
            blockLuma = (float)lightmap[(int)curSprite->pos.x][(int)curSprite->pos.y].a / 256;

        spritePos.x = curSprite->pos.x - p.x;
        spritePos.y = curSprite->pos.y - p.y;


        invDet = 1.0 / (plane.x * d.y - d.x * plane.y);

        transform.x = invDet * (d.y * spritePos.x - d.x * spritePos.y);
        transform.y = invDet * (-plane.y * spritePos.x + plane.x * spritePos.y);

        spriteScreenX = (int)((w / 2) * (1 + transform.x / transform.y));

        spriteHeight = abs((int)(h / transform.y));

        zHeight = curSprite->zHeight / transform.y;

        drawStart.y = -spriteHeight / 2 + h / 2 + zHeight;
        if (drawStart.y < 0) drawStart.y = 0;
        drawEnd.y = spriteHeight / 2 + h / 2 + zHeight;
        if (drawEnd.y >= h) drawEnd.y = h - 1;

        spriteWidth = spriteHeight;
        drawStart.x = -spriteWidth / 2 + spriteScreenX;
        dsxold = drawStart.x;
        if (drawStart.x < 0) drawStart.x = 0;
        drawEnd.x = spriteWidth / 2 + spriteScreenX;
        if (drawEnd.x >= w) drawEnd.x = w - 1;

        if (fogEnabled)
            fogCache = r_fog_multiplier(sqrt(sdi[i].distance),
                                        fogDistance, fogIntensity);

        for (int x = drawStart.x; x < drawEnd.x; ++x) {

            currentRay = (ri->rays[x] + 0);

            texPos.x = (int)(256 * (x - dsxold) * tex->width / spriteWidth) / 256;
            if (texPos.x < 0) goto r_skip_sprite_rendering;

            if (transform.y > 0 && transform.y < currentRay->dist) {
                // galaxy brain way to detect hit
                if (x == w / 2 && (curSprite->flags & R_SPRITEFLAG_HIT)) {
                    r->hit.type = R_HIT_SPRITE;
                    r->hit.info.sprite = curSprite;
                }
                for (int y = drawStart.y; y < drawEnd.y; ++y) {
                    tmp = (y - zHeight) * 256 - h * 128 + spriteHeight * 128;
                    texPos.y = ((tmp * (tex->height-1)) / spriteHeight) / 256;
                    if (texPos.y < 0) goto r_skip_sprite_rendering;

                    color = tex->pixels[texPos.y][texPos.x];
                    r_color_mul(&color, blockLuma);
                    if (color.a > 0) {
                        if (fogEnabled)
                            r_fog_apply(&color, fogColor, fogCache);
                        buffer[y][x] = color;
                    }
                }
            }
        }
    r_skip_sprite_rendering:
        continue;
    }
}
