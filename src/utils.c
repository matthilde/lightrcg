/*
 * Raycaster by matthilde
 *
 * utils.c
 *
 * Utility functions will be added here
 */
#include <stdlib.h>

#include <lightrcg/lightrcg.h>

int _R_sort_cmp(const void* v1, const void* v2) {
    // wow this is a long name
    struct _rsprite_distinfo a = *((struct _rsprite_distinfo*)v1);
    struct _rsprite_distinfo b = *((struct _rsprite_distinfo*)v2);
    return a.distance < b.distance ? -1 : 1;
}
void R_SortSpriteOrder(RaySpriteBuffer* sb) {
    int s = sb->used;
    struct _rsprite_distinfo *info = sb->info;

    qsort(info, s, sizeof(struct _rsprite_distinfo), _R_sort_cmp);
}
