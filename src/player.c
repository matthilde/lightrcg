/*
 * Raycaster by matthilde
 *
 * player.c
 *
 * Functions to make the player move around.
 */
#include <math.h>
#include <lightrcg/lightrcg.h>

static int cos90 = cos(PI / 2);
static int sin90 = sin(PI / 2);

// Allows moving the player forward and backward
void R_PlayerMoveForward(Raycaster *r, double step) {
    double px = r->player.x, py = r->player.y;
    double vx = px + step * r->direction.x;
    double vy = py + step * r->direction.y;

    // if (r->map->buffer[(int)py][(int)vx] == 0) r->player.x = vx;
    // if (r->map->buffer[(int)vy][(int)px] == 0) r->player.y = vy;

    // wow new collision system
    if (!(r->map->buffer[(int)vx][(int)py].flags & R_BLOCKFLAG_COLLIDE))
        r->player.x = vx;
    if (!(r->map->buffer[(int)px][(int)vy].flags & R_BLOCKFLAG_COLLIDE))
        r->player.y = vy;
}

// No collision version
void R_PlayerMoveForwardNC(Raycaster *r, double step) {
    r->player.x += step * r->direction.x;
    r->player.y += step * r->direction.y;
}

// Allows moving the player forward and backward
void R_PlayerMoveSideway(Raycaster *r, double step) {
    Vector dir;
    dir.x = r->direction.x * cos90 - r->direction.y * sin90;
    dir.y = r->direction.x * sin90 + r->direction.y * cos90;
    double px = r->player.x, py = r->player.y;
    double vx = px - step * dir.x;
    double vy = py - step * dir.y;

    // if (r->map->buffer[(int)py][(int)vx] == 0) r->player.x = vx;
    // if (r->map->buffer[(int)vy][(int)px] == 0) r->player.y = vy;

    // wow new collision system
    if (!(r->map->buffer[(int)vx][(int)py].flags & R_BLOCKFLAG_COLLIDE))
        r->player.x = vx;
    if (!(r->map->buffer[(int)px][(int)vy].flags & R_BLOCKFLAG_COLLIDE))
        r->player.y = vy;
}

// No collision version
void R_PlayerMoveSidewayNC(Raycaster *r, double step) {
    Vector dir;
    dir.x = r->direction.x * cos90 - r->direction.y * sin90;
    dir.y = r->direction.x * sin90 + r->direction.y * cos90;
    r->player.x -= step * dir.x;
    r->player.y -= step * dir.y;
}

// Rotates the player. Left or right.
void R_PlayerRotate(Raycaster *r, double angle) {
    double oldDirX = r->direction.x;
    double oldPlaneX = r->plane.x;

    double degAngle = angle * (PI/180);
    r->angle = fmod(r->angle + angle, 360.0);
    r->angle = fmod(r->angle + 360.0, 360.0);

    r->direction.x = r->direction.x * cos(degAngle) - r->direction.y * sin(degAngle);
    r->direction.y = oldDirX * sin(degAngle) + r->direction.y * cos(degAngle);
    r->plane.x = r->plane.x * cos(degAngle) - r->plane.y * sin(degAngle);
    r->plane.y = oldPlaneX * sin(degAngle) + r->plane.y * cos(degAngle);

}
