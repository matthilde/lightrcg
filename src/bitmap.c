/*
 * lightrcg by matthilde
 *
 * bitmap.c
 *
 * Contains functions to load bitmap files. The engine uses a simple
 * custom format to load bitmaps.
 *
 * Works like this:
 * [MAGIC NUMBER: 'LRBMP\x01']
 * <short (big endian): amount of contained bitmaps>
 * <short: width> <short: height>
 * <byte: r> <byte: g> <byte: b> <byte: a> [...]
 * <short: width> <short: height>
 * <byte: r> <byte: g> <byte: b> <byte: a> [...]
 * [EOF]
 */
#include <stdio.h>
#include <stddef.h>

#include <stdlib.h>

#include <lightrcg/lightrcg.h>

static const char* _magicnum = "LRBMP\x01";
static const int   _magiclen = 6;

static int checkMagicNumber(FILE *f) {
    char buf[6];
    int len = fread(buf, sizeof(char), _magiclen, f);
    if (len != _magiclen) return 0;
    // can't be bothered to import string.h lol
    for (int i = 0; i < _magiclen; ++i)
        if (buf[i] != _magicnum[i]) return 0;
    return 1;
}

static int fgetint(FILE* f) {
    int a = fgetc(f);
    int b = fgetc(f);
    
    if (a == EOF || b == EOF) return EOF;
    
    return a << 8 | b;
}
static Color fgetcolor(FILE* f) {
    Color c;
    c.r = fgetc(f);
    c.g = fgetc(f);
    c.b = fgetc(f);
    c.a = fgetc(f);
    return c;
}

// returns NULL on error
RayBitmapFile* R_OpenBitmap(const char* filename) {
    FILE *f = fopen(filename, "rb");
    if (!f)
        return NULL;
    int isMagicWoohoo = checkMagicNumber(f);
    if (!isMagicWoohoo) {
        fclose(f);
        puts("wait that's supposed to be valid...");
        return NULL;
    }

    RayBitmapFile* bmp = STRUCTALLOC(RayBitmapFile);
    bmp->file = f;
    bmp->size = fgetint(f); bmp->nth = 0;

    return bmp;
}
void R_CloseBitmap(RayBitmapFile* f) {
    fclose(f->file);
    free(f);
}
// read one bitmap
RayBuffer* R_ReadBitmap(RayBitmapFile* bmp) {
    if (bmp->nth >= bmp->size) return NULL;
    ++(bmp->nth);
    FILE *f = bmp->file;
    long t = ftell(f);

    int w = fgetint(f); int h = fgetint(f);
    if (w == EOF || h == EOF)
        return NULL;
    RayBuffer* buf = R_NewFramebuffer(w, h);
    Color *pix = buf->pixels[0];
    int i;
    // for (i = 0; i < w*h; ++i) pix[i] = fgetcolor(f);
    
    size_t s = w * h;
    for (size_t r = s; r > 0;) {
        size_t rr = fread(pix, sizeof(Color), r, f);
        if (rr == 0) {
            perror("what");
            return NULL;
        }
        r -= rr;
    }
    return buf;
}

// load a font
RayFont* R_LoadFont(const char* filename, byte start) {
    RayBitmapFile* bmp = R_OpenBitmap(filename);
    if (!bmp) return NULL;

    RayFont* font = STRUCTALLOC(RayFont);

    font->start = start;
    font->end   = start + bmp->size;
    font->chars = (RayBuffer**)xmalloc(sizeof(RayBuffer*) * bmp->size);

    for (int i = 0; i < bmp->size; ++i) {
        font->chars[i] = R_ReadBitmap(bmp);
        if (i == 0) {
            // font height will be assumed on the first font bitmap.
            // because font height is assumed to be consistent.
            font->fontHeight = font->chars[i]->height;
        }
    }

    R_CloseBitmap(bmp);

    return font;
}
// free a font
void R_FreeFont(RayFont* font) {
    for (int i = 0; i < font->end - font->start; ++i)
        free(font->chars[i]);
    free(font->chars);
    free(font);
}

// Put a bitmap on a framebuffer
// buf is the framebuffer, b is the bitmap to show
void R_ShowBitmap(RayBuffer* buf, RayBuffer* b, Point pos) {
    Color** bufpix = buf->pixels;
    Color** bmppix = b->pixels;
    int ox, oy;
    int w = buf->width, h = buf->height;
    // int bw = b->width, bh = b->height;
    for (int y = 0; y < b->height; ++y) {
        oy = y + pos.y;
        if (oy < 0 || oy >= h) continue;
        for (int x = 0; x < b->width; ++x) {
            ox = x + pos.x;
            if (ox < 0 || ox >= w) continue;

            Color c = bmppix[y][x];
            if (c.a != 0) bufpix[oy][ox] = c;
        }
    }
}

// Write text on a framebuffer
void R_WriteText(RayBuffer* buf, RayFont* font, Point p, const char* s) {
    char c; Point np = p;
    int start = font->start, end = font->end, fh = font->fontHeight;
    RayBuffer** chars = font->chars; RayBuffer* curChar;

    for (; *s != 0; s++) {
        c = *s;
        if (c == '\n') {
            p.x = np.x;
            p.y += fh;
            continue;
        } else if (c < start || c >= end) continue;

        curChar = chars[c - start];
        R_ShowBitmap(buf, curChar, p);
        p.x += curChar->width;
    }
}

//////////// RayTexture stuff
RayTexture* R_NewTexture(int frames, int animated, int interval) {
    RayTexture* tex = STRUCTALLOC(RayTexture);
    tex->animated     = animated > 0;
    tex->frameCount   = frames;
    tex->currentFrame = 0;
    tex->interval     = interval;
    tex->ticks        = 0;
    // tex->frames       = (RayBuffer**)xmalloc(sizeof(RayBuffer*) * frames);
    if (tex->frameCount > 1)
        tex->f.rames = (RayBuffer**)xmalloc(sizeof(RayBuffer*) *
                                            frames);
    else
        tex->f.rame  = NULL;

    return tex;
}
// Since buffers are not allocated by R_NewTexture, it is assumed the
// developer frees the frames themselves.
void R_FreeTexture(RayTexture* tex) {
    if (tex->frameCount > 1) free(tex->f.rames);
    free(tex);
}
// set the limit to 0 if you don't want to limit the amount of frames
// loaded.
//
// returns NULL if it fails to load.
RayTexture* R_LoadBitmapTexture(RayBitmapFile* bmp,
                                int interval, int limit) {

    int maxFrames = bmp->size - bmp->nth;
    limit = limit <= 0 ? maxFrames : MIN(maxFrames, limit);
    RayTexture *tex = R_NewTexture(limit, limit > 1, interval);
    RayBuffer* frame;
    if (limit > 1) {
        for (int i = 0; i < limit; ++i) {
            
            frame = R_ReadBitmap(bmp);
            if (!frame)
                return NULL;
            tex->f.rames[i] = frame;
        }
    } else {
        frame = R_ReadBitmap(bmp);
        if (!frame) return NULL;
        tex->f.rame = frame;
    }

    return tex;
}

RayTexture* R_LoadBitmapTextureFN(const char* fn, int interval) {
    RayBitmapFile* bmp = R_OpenBitmap(fn);
    if (!bmp) return NULL;
    RayTexture* tex = R_LoadBitmapTexture(bmp, interval, 0);
    R_CloseBitmap(bmp);
    return tex;
}

// get current texture
RayBuffer* R_GetTextureBuffer(RayTexture* tex) {
    if (tex->frameCount > 1)
        return tex->f.rames[tex->currentFrame];
    else
        return tex->f.rame;
}

// update texture frame number
void R_UpdateTextureFrame(Raycaster* r, RayTexture* tex) {
    if (tex->animated) {
        tex->ticks += r->lastFrameTicks;
        tex->currentFrame = (tex->ticks / tex->interval) % tex->frameCount;
    }
}

// displays a texture to a framebuffer
void R_ShowTexture(RayBuffer* buf, RayTexture* tex, Point pos) {
    R_ShowBitmap(buf, R_GetTextureBuffer(tex), pos);
}
