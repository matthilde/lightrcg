/*
 * lightrcg by matthilde
 *
 * mapdata.c
 *
 * Map data format to store maps (including layers, light, etc.) in
 * files. This is a custom format to load and save maps.
 *
 * Works like this:
 * [MAGIC NUMBER: 'LRMAP\x01']
 * <short: width> <short: height> <byte: layer count>
 * [ MAP LAYER (*layer count) ] <byte: tile> <byte: flags> [...]
 * [ LIGHT MAP ] <byte: luma> <byte: luma> [...]
 * [ FLOOR/CEILING MAP ] <byte: floortile> <byte: ceilingtile> [...]
 * [EOF]
 */
#include <stdio.h>

#include <lightrcg/lightrcg.h>

static const char* _magicnum = "LRMAP\x01";
static const int   _magiclen = 6;

static int checkMagicNumber(FILE *f) {
    char buf[6];
    int len = fread(buf, sizeof(char), _magiclen, f);
    if (len != _magiclen) return 0;
    // can't be bothered to import string.h lol
    for (int i = 0; i < _magiclen; ++i)
        if (buf[i] != _magicnum[i]) return 0;
    return 1;
}

static int fgetint(FILE* f) {
    byte a = fgetc(f);
    byte b = fgetc(f);

    return a << 8 | b;
}
static void fputint(FILE* f, int i) {
    byte a = (i & 0xff00) >> 8;
    byte b =  i & 0x00ff;
    fputc(a, f); fputc(b, f);
}

RayMap* R_LoadMapFile(const char* filename) {
    FILE* f = fopen(filename, "r");
    if (!f) return NULL;
    if (!checkMagicNumber(f)) return NULL;

    int width = fgetint(f); int height = fgetint(f);
    /* int layers = */ fgetc(f); // layers (to be implemented)

    RayMap* m = R_NewMap(width, height);
    for (int i = 0; i < (width * height); ++i) {
        m->buffer[0][i].b = fgetc(f);
        m->buffer[0][i].flags = fgetc(f);
    }
    for (int i = 0; i < (width * height); ++i) {
        m->floormap[0][i].floor   = fgetc(f);
        m->floormap[0][i].ceiling = fgetc(f);
    }
    for (int i = 0; i < (width * height); ++i)
        m->lightmap[0][i].a = fgetc(f);

    fclose(f);

    return m;
}

int R_SaveMapFile(RayMap* m, const char* filename) {
    FILE *f = fopen(filename, "w");
    if (!f) return -1;
    
    fputs(_magicnum, f);
    fputint(f, m->width); fputint(f, m->height); fputc(1, f);

    for (int i = 0; i < (m->width * m->height); ++i) {
        fputc(m->buffer[0][i].b, f);
        fputc(m->buffer[0][i].flags, f);
    }
    for (int i = 0; i < (m->width * m->height); ++i) {
        fputc(m->floormap[0][i].floor, f);
        fputc(m->floormap[0][i].ceiling, f);
    }
    for (int i = 0; i < (m->width * m->height); ++i)
        fputc(m->lightmap[0][i].a, f);

    fclose(f);
    return 0;
}
