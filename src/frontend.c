/*
 * Raycaster by matthilde
 *
 * frontend.c
 *
 * A simple SDL Front-End to allow users making simple applications
 * using the raycaster. However you are completely free to use
 * anything you want however you want :)
 *
 * This could also be used as a series of example that shows how you
 * can make your own application without depending on this frontend.
 */
#include <stdlib.h>
#include <sys/time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>

#include <lightrcg/lightrcg.h>


#define FELOG(...) SDL_Log("[RF] " __VA_ARGS__)
#define FEDIE(...) do { \
        FELOG("[FATAL] " __VA_ARGS__); \
        RF_End(); \
        exit(1); } while(0)

// #define TEXBUFFER_SIZE 64 // Texture bank buffer size.

// Global variables of everything needed to make cool stuff.
SDL_Event event;
SDL_Window* win;
SDL_Renderer *renderer;
// The texture we will use to dump our raycaster's framebuffer.
SDL_Texture *rendertex;
Raycaster* raycast;
RaySpriteBuffer* spritebuf;

// Window size and game size.
// If the game buffer size is smaller, it will just stretch out.
// You can for example divide the window size by 2 to get a pixel
// effect!
int winWidth, winHeight, gameWidth, gameHeight, framerate;
int texBufsize, texStackptr, isRunning;

// To calculate the correct framerate
int deltaTime = 0, frameTime = 0;
struct timeval timeStart, timeEnd;

// Player movement configuration
struct {
    int    useMouse;
    double speed, aimSpeed;
} moveConfig = {
    .useMouse = 0,
    .speed = 5.0, .aimSpeed = 6.0
};

// other needed variables
Vector playerVelocity; double angularVelocity;

// Initializes SDL
void RF_Init(const char* title, int fps, int sbsize,
                   int w, int h, int gw, int gh) {
    FELOG("Initializing everything...");

    winWidth = w; winHeight = h;
    gameWidth = gw; gameHeight = gh;
    framerate = 1000 / fps; // -1 for max FPS
    isRunning = 1;
    gettimeofday(&timeStart, NULL);

    // Initializing SDL and create the window and the renderer.
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) sdl_die("SDL_Init");
    SDL_CreateWindowAndRenderer(w, h, 0, &win, &renderer);
    SDL_SetWindowTitle(win, title); // sets fancy title :)

    // Initializing the texture.
    rendertex = SDL_CreateTexture(renderer,
                                  SDL_PIXELFORMAT_RGBA32,
                                  SDL_TEXTUREACCESS_STREAMING,
                                  gw, gh);
    if (rendertex == NULL) sdl_die("SDL_CreateTexture");

    // Initializing the raycasting stuff and allocating needed shit.
    texBufsize = 256; texStackptr = 0;

    raycast = R_NewRaycaster(gw, gh);
    raycast->textures = (RayTexture**)xmalloc(sizeof(RayTexture*) * texBufsize);

    // Initializing the sprite buffer
    spritebuf = R_NewSpriteBuffer(sbsize);

    playerVelocity.x = 0; playerVelocity.y = 0;
    angularVelocity = 0;

    FELOG("Initialized everything!");
}

// Frees everything and do some cleanup
void RF_End() {
    FELOG("Cleaning up everything...");
    
    // Free texture bank buffer
    for (int i = 0; i < texStackptr; ++i) {
        // R_FreeFramebuffer(raycast->textures[i].f.rame);
        RayTexture* tex = raycast->textures[i];
        if (tex->frameCount == 1)
            R_FreeFramebuffer(tex->f.rame);
        else {
            for (u32 i = 0; i < tex->frameCount; ++i)
                R_FreeFramebuffer(tex->f.rames[i]);
        }
        R_FreeTexture(raycast->textures[i]);
    }
    free(raycast->textures);
    // Free el raycaster
    R_FreeRaycaster(raycast);
    // Free the sprite buffer
    R_FreeSpriteBuffer(spritebuf);

    // Destroy all the SDL stuff and quit SDL
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(win);
    SDL_Quit();

    FELOG("Goodbye!");
}

// Checks if it still should be running
int RF_Running() { return isRunning; }
// requests stop (except it's not a bus)
void RF_StopRunning() { isRunning = 0; }

//////// EVENT STUFF
// Set if you want to aim with the mouse
void RF_SetMouseAiming(int enable) {
    SDL_SetRelativeMouseMode(enable);
    moveConfig.useMouse = enable;
}
// speedy speed!
void RF_SetPlayerSpeed(double speed) {
    moveConfig.speed = speed;
}
void RF_SetAimingSpeed(double speed) {
    moveConfig.aimSpeed = speed;
}

// You can add this to your code then add extra keybindings.
// RF_BasicEventHandle() is kinda deprecated now that exists.
int RF_HandlePlayerMovement(SDL_Event *event) {
    int didSomething = 1;
    double *adVars[2] = { &angularVelocity, &(playerVelocity.x) };
    double spVars[2] = { moveConfig.aimSpeed, moveConfig.speed };

    switch (event->type) {
    case SDL_KEYDOWN:
        switch (event->key.keysym.sym) {
        case SDLK_d: // angularVelocity = -moveConfig.aimSpeed; break;
            adVars[moveConfig.useMouse][0] = spVars[moveConfig.useMouse];
            // case SDLK_a: angularVelocity = moveConfig.aimSpeed;  break;
            break;
        case SDLK_a:
            adVars[moveConfig.useMouse][0] = -spVars[moveConfig.useMouse];
            break;
        case SDLK_w: playerVelocity.y = moveConfig.speed;  break;
        case SDLK_s: playerVelocity.y = -moveConfig.speed; break;
        default: didSomething = 0;
        }
        break;
    case SDL_KEYUP:
        switch (event->key.keysym.sym) {
        case SDLK_d: case SDLK_a:
            adVars[moveConfig.useMouse][0] = 0.0;
            break;
        case SDLK_w: case SDLK_s: playerVelocity.y = 0.0; break;
        default: didSomething = 0;
        }
        break;
    case SDL_MOUSEMOTION:
        if (moveConfig.useMouse) {
            angularVelocity = event->motion.xrel * -(moveConfig.aimSpeed);
        }
        break;
    default: didSomething = 0; break;
    }
    return didSomething;
}
// int RF_PollEvent(RFEvent* event) { }

//////// PLAYER STUFF
void RF_SetPlayerVelocityX(double x) {
    playerVelocity.x = x;
}
void RF_SetPlayerVelocityY(double y) {
    playerVelocity.y = y;
}
void RF_SetPlayerAngVelocity(double angle) {
    angularVelocity = -angle;
}
void RF_SetPlayerPosition(Vector xy) {
    raycast->player = xy;
}

//////// SETTINGS STUFF
void RF_SetFogEnabled(int enabled) {
    raycast->fogEnabled = enabled > 0;
}
void RF_SetFog(Color c, float distance, float intensity) {
    raycast->fogColor     = c;
    raycast->fogDistance  = distance;
    raycast->fogIntensity = intensity;
}

void RF_SetFramerate(int fps) {
    framerate = 1000 / fps;
}

//////// UPDATE STUFF
void RF_ClearFramebuffer(byte r, byte g, byte b) {
    R_ClearFramebuffer(raycast->buffer, RGB(r, g, b));
}

// floor/ceiling update
void RF_UpdateHorizon() {
    R_RenderPlate(raycast);
}
// walls update
void RF_UpdateWalls() {
    // update texture
    R_CalculateWalls(raycast);
    R_RenderRaycastInfo(raycast);
    
}
void RF_UpdateSprites() {
    R_RenderSprites(raycast, spritebuf);
}
void RF_UpdatePlayer(int collides) {
    float ft = (float)frameTime / 1000;
    if (angularVelocity)
        R_PlayerRotate(raycast,
                       angularVelocity * 2.0 * ft);
    if (playerVelocity.x) {
        if (collides)
            R_PlayerMoveSideway(raycast,
                                playerVelocity.x * ft);
        else
            R_PlayerMoveSidewayNC(raycast,
                                playerVelocity.x * ft);
    }
    if (playerVelocity.y) {
        if (collides)
            R_PlayerMoveForward(raycast,
                                playerVelocity.y * ft);
        else
            R_PlayerMoveForwardNC(raycast,
                                  playerVelocity.y * ft);
    }

    angularVelocity *= !(moveConfig.useMouse);
}
// Function that updates everything (so you don't have to do it)
void RF_Update() {
    // update player position
    // ((float)(framerate - deltaTime) / 1000));
    // * ((float)(framerate - deltaTime) / 1000));
    void* texturePixels; int texturePitch = 0;
    if (SDL_LockTexture(rendertex, NULL,
                        &texturePixels, &texturePitch) != 0) {
        FEDIE("Failed to lock render texture during update.");
    }
    else {
        memcpy(texturePixels,
               raycast->buffer->pixels[0],
               texturePitch * gameHeight);
    }
    SDL_UnlockTexture(rendertex);

    // draw everything
    SDL_RenderClear(renderer);
    // SDL_RenderCopyEx(renderer, rendertex, NULL, &dstrect, 90, NULL, SDL_FLIP_NONE);
    SDL_RenderCopy(renderer, rendertex, NULL, NULL);
    SDL_RenderPresent(renderer);
                        
    // get the time stuff
    gettimeofday(&timeEnd, NULL);
    deltaTime  = (timeEnd.tv_sec - timeStart.tv_sec) * 1000;
    deltaTime += (timeEnd.tv_usec - timeStart.tv_usec) / 1000;

    // tick tick tick tick...
    if (deltaTime < framerate) {
        frameTime = framerate;
        SDL_Delay(framerate - deltaTime);
    } else
        frameTime = deltaTime;

    raycast->lastFrameTicks = frameTime;
    raycast->ticks += frameTime;
    for (int i = 0; i < texStackptr; ++i)
        R_UpdateTextureFrame(raycast, raycast->textures[i]);
    /*
    for (int i = 0; i < spritebuf; ++i)
        R_UpdateTextureFrame(r, sprbuf->sprites[i].
    */

    gettimeofday(&timeStart, NULL);
}
void RF_UpdateRender() {
    RF_UpdateHorizon();
    RF_UpdateWalls();
    RF_UpdateSprites();
}
void RF_UpdateAll(int collides) {
    RF_UpdateHorizon();
    RF_UpdateWalls();
    RF_UpdateSprites();
    RF_UpdatePlayer(collides);
    RF_Update();
}

//////// GET FUNCTIONS
// Allows user to get variables used here
SDL_Window*   RF_GetWindow() { return win; }
SDL_Renderer* RF_GetRenderer() { return renderer; }
SDL_Texture*  RF_GetRendertex() { return rendertex; }
Raycaster*    RF_GetRaycaster() { return raycast; }
RaycastInfo*  RF_GetRayinfo() { return raycast->zbuffer; }
RayTexture**  RF_GetTextures() { return raycast->textures; }
RayTexture*   RF_GetTexture(int id) { return raycast->textures[id]; }
RayMap*       RF_GetLoadedMap() { return raycast->map; }
RaySpriteBuffer* RF_GetSpriteBuffer() { return spritebuf; }
RaySprite*    RF_GetSprite(int id) { return (spritebuf->sprites + id); }
double        RF_GetFPS() {
    // return frameTime > 0 ? 1.0 / (0.001 * (float)frameTime) : 0.0;
    // float ft = 1000.0 / (float)frameTime;
    if (frameTime <= 0)
        return -1.0;
    else
        return 1000.0 / (double)frameTime;
}

//////// SPRITE STUFF
void RF_ResetSpriteBuf() {
    for (int i = 0; i < spritebuf->used; ++i)
        spritebuf->sprites[i].visible = 0;
    spritebuf->used = 0;
}
int RF_PushSprite(int texid, Vector pos, byte flags) {
    FELOG("Pushing sprite with texture #%d at (%.1f %.1f)",
          texid, pos.x, pos.y);
    RayTexture* tex = raycast->textures[texid];
    int id = R_PushSprite(spritebuf, tex, pos, flags);
    if (id == -1)
        FEDIE("Failed to push sprite.");

    return id;
}
int RF_PushDirSprite(int texid, int dirs, Vector pos, byte flags) {
    FELOG("Pushing directional sprite with texture #%d "
          "at (%.1f %.1f)", texid, pos.x, pos.y);
    RayTexture** tex = (raycast->textures + texid);
    int id = R_PushDirSprite(spritebuf, tex, dirs, pos, flags);
    if (id == -1)
        FEDIE("Failed to push directional sprite.");

    return id;
}

//////// BITMAP STUFF

void RF_WriteText(RayFont* font, Point pos, const char* s) {
    R_WriteText(raycast->buffer, font, pos, s);
}

void RF_ShowBitmap(RayBuffer* buf, Point pos) {
    R_ShowBitmap(raycast->buffer, buf, pos);
}

void RF_ShowTexture(RayTexture* tex, Point pos) {
    R_ShowTexture(raycast->buffer, tex, pos);
}

void RF_ShowTextureByID(int id, Point pos) {
    R_ShowTexture(raycast->buffer, raycast->textures[id], pos);
}

//////// TEXTURE STUFF
// Will return stack position on success
static int texAllocate(int count) {
    int newptr = texStackptr + count;
    /*
      if (newptr >= TEXBUFFER_SIZE)
        FEDIE("Failed to allocate textures.");
    */
    if (newptr >= texBufsize) {
        while (newptr > texBufsize) texBufsize *= 2;
        FELOG("Resizing to %d textures (%d/%d)", texBufsize,
              newptr, texBufsize);
        raycast->textures = (RayTexture**)xrealloc(raycast->textures,
            sizeof(RayTexture*) * texBufsize);
    }

    texStackptr = newptr;
    return newptr;
}
int RF_LoadTextures(const char* filename) {
    FELOG("Loading lightrcg bitmap...");

    RayBitmapFile* bmp = R_OpenBitmap(filename);
    if (!bmp)
        FEDIE("Failed to load bitmap '%s'", filename);

    int oldptr = texStackptr; int newptr = texAllocate(bmp->size);
    for (int i = oldptr; i < newptr; ++i)
        raycast->textures[i] = R_LoadBitmapTexture(bmp, 0, 1);

    R_CloseBitmap(bmp);
    return oldptr;
}
int RF_LoadAnimTexture(const char* filename, int interval) {
    FELOG("Loading lightrcg bitmap...");

    RayTexture* t = R_LoadBitmapTextureFN(filename, interval);
    if (!t)
        FEDIE("Failed to load bitmap '%s'", filename);
    int oldptr = texStackptr; texAllocate(1);
    raycast->textures[oldptr] = t;
    
    return oldptr;
}
int RF_LoadTextureFromBmp(RayBitmapFile* bmp, int frames, int interval) {
    FELOG("Loading %d frames from bitmap...", frames);

    RayTexture* t = R_LoadBitmapTexture(bmp, interval, frames);
    if (!t)
        FEDIE("Failed to load bitmap!");
    int oldptr = texStackptr; texAllocate(1);
    raycast->textures[oldptr] = t;

    return oldptr;
}
int RF_PushNewTextures(int count, int w, int h) {
    FELOG("Pushing %d textures...", count);
    int oldptr = texStackptr; int newptr = texAllocate(count);

    for (int i = oldptr; i < newptr; ++i) {
        raycast->textures[i] = R_NewTexture(1, 0, 0);
        raycast->textures[i]->f.rame = R_NewFramebuffer(w, h);
    }
    return oldptr;
}


//////// MAP STUFF
// This is now deprecated. I mean you can still use it, it's just
// bad.
void RF_LoadSimpleMap(const char* filename) {
    FELOG("Loading map '%s'...", filename);
    raycast->map = R_LoadSimpleMap(filename);
    if (!raycast->map)
        FEDIE("Unable to load map '%s'", filename);
}
// Use the new map format now.
void RF_LoadMapFile(const char* filename) {
    // It is recommended to unload the previous map before loading a
    // new one. It is up to the programmer tho, if they wish to keep
    // the map for later.
    RayMap* m = R_LoadMapFile(filename);
    if (!m)
        FEDIE("Failed to load map '%s'", filename);
    FELOG("Map '%s' loaded!", filename);

    raycast->map = m;
}
void RF_SaveMapFile(const char* filename) {
    if (R_SaveMapFile(raycast->map, filename) == -1)
        FEDIE("Failed to save map '%s'", filename);
    FELOG("Map '%s' saved!", filename);
}
RayMap* RF_CreateMap(int w, int h) {
    FELOG("Creating a new map of size %dx%d", w, h);
    raycast->map = R_NewMap(w, h);
    if (!raycast->map)
        FEDIE("Unable to create a new map.");

    return raycast->map;
}
void RF_UnloadMap() {
    if (raycast->map) {
        R_FreeMap(raycast->map);
        raycast->map = NULL;
    }
}
