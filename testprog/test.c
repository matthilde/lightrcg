#define SDL_MAIN_HANDLED
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <math.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>

#include <lightrcg/lightrcg.h>

#define TSIZE 48

#define GAME_W 320
#define GAME_H 240

void printInformation(Raycaster* r, RayFont *font) {
    const char* _dir_str[] = {"right", "down", "left", "up"};
    const char* fmt = "wallface: %s\nplayer: %.1f %.1f\n%d FPS"
        "\nhit: %s";
    char buf[256], bufb[128];

    R_Direction d = r->zbuffer->rays[GAME_W / 2][0].dir;
    int fps = RF_GetFPS();

    switch (r->hit.type) {
    case R_HIT_BLOCK:
        sprintf(bufb, "block (%d %d) #%d",
                r->hit.info.block.x, r->hit.info.block.y,
                r->map->buffer[r->hit.info.block.x][r->hit.info.block.y].b);
        break;
    case R_HIT_SPRITE:
        sprintf(bufb, "sprite 0x%08x",
                (unsigned long)r->hit.info.sprite);
        break;
    case R_HIT_NONE:
        sprintf(bufb, "none");
    }

    sprintf(buf, fmt, _dir_str[d],
            r->player.x, r->player.y, fps, bufb);

    RF_WriteText(font, P{0, 0}, buf);
}

#define MAP_W 16
#define MAP_H 16
void mapgen(RayMap* m) {
    for (int x = 0; x < MAP_W; ++x)
        for (int y = 0; y < MAP_H; ++y) {
            // m->floormap[x][y].floor = rand() % TSIZE + 1;
            // m->floormap[x][y].ceiling = rand() % TSIZE + 1;

            // floormap
            m->floormap[x][y] = (RayFC){14, 0};

            // lightmap
            /*
            m->lightmap[x][y].a = (256 / MAP_H) * y;
            if (x > 2 && x < 6 && y > 2 && y < 6)
                m->lightmap[x][y].a = 255;
            */
            m->lightmap[x][y].a = 255;

            // tilemap
            if (x == 0 || x == MAP_W - 1 ||
                y == 0 || y == MAP_H - 1)
                m->buffer[x][y] = (RayTile){15, 1};
            else {
                m->buffer[x][y] = (RayTile){0, 0};
                continue;
                int r = rand() % 3;
                if (r == 0)
                    m->buffer[x][y] = (RayTile){rand() % TSIZE + 1, 1};
                else
                    m->buffer[x][y] = (RayTile){0, 0};
            }
        }
    // m->buffer[5][4] = (RayTile){ 10, 0 };
    // m->buffer[5][6] = (RayTile){ 10, 0 };
    // m->buffer[5][5] = (RayTile){ 10, R_BLOCKFLAG_THIN };
}

int main(int argc, char *argv[]) {
    int fog = 0, limit30 = 0, selBlock = 0;
    SDL_Event event;
    RF_Init("lightrcg", 60, 32, 640, 480, GAME_W, GAME_H);
    RF_SetFogEnabled(fog);
    RF_SetFog(RGB(255, 200, 255), 1, 0.5);

    // load map
    RayMap* m = RF_CreateMap(MAP_W, MAP_H);
    mapgen(m);

    // push and generate textures
    // RayBuffer** textures = RF_GetTextures();
    RF_LoadTextures("assets/tiles.tex");

    // int pillar = RF_PushNewTextures(1, 64, 64);
    // loadTexture("assets/old/pillar.tex", textures, pillar, 64, 64);
    int cursor = RF_LoadTextures("assets/cursor.tex");
    int sky    = RF_LoadTextures("assets/sky.tex");

    Raycaster *r = RF_GetRaycaster();

    RaySprite *spr = RF_GetSprite(RF_PushSprite(cursor, V{1.0, 1.0}, 0));
    Point ppos;

    int dirSpriteTex = RF_LoadTextures("assets/debugball.tex");
    int dirSpriteID = RF_PushDirSprite(dirSpriteTex, 8, V{8.0, 8.0}, 0);
    RaySprite* dirSprite = RF_GetSprite(dirSpriteID);
    float *dsa = &(RF_GetSprite(dirSpriteID)->angle);

    int skeletonTex = RF_LoadAnimTexture("assets/skele.tex", 40);
    // r->map->buffer[4][4] = (RayTile){ skeleton+1, 1 };
    RaySprite* skele = RF_GetSprite(
        RF_PushSprite(skeletonTex, V{4.0, 4.0}, R_SPRITEFLAG_HIT));
    // RF_PushNewTextures(420, 1, 1); // lol

    RayFont* font = R_LoadFont("assets/fontsmall.tex", 0);
    if (!font) {
        perror("R_LoadFont");
        return 1;
    }

    int t = 0; float pi180 = 3.1415 / 180;
    float sintable[360], costable[360];
    for (int i = 0; i < 360; ++i) {
        sintable[i] = sin(pi180 * i);
        costable[i] = cos(pi180 * i);
    }
    while (RF_Running()) {
        t = (t + 1) % 360;
        skele->pos.x = 10.0 + sintable[t];
        skele->pos.y = 10.0 + costable[t];

        dirSprite->zHeight = sintable[(t * 2) % 360] * 16;

        // *dsa += 2.0;
        ppos.x = r->player.x + r->direction.x * 3;
        ppos.y = r->player.y + r->direction.y * 3;
        spr->pos.x = ppos.x + 0.5; spr->pos.y = ppos.y + 0.5;
        while (SDL_PollEvent(&event)) {
            if (!RF_HandlePlayerMovement(&event) &&
                event.type == SDL_KEYUP)
                switch (event.key.keysym.sym) {
                case SDLK_g:
                    RF_PushSprite(dirSpriteTex, r->player, 0);
                    break;
                case SDLK_q:
                    m->buffer[ppos.x][ppos.y] = (RayTile){selBlock + 1,1};
                    break;
                case SDLK_e: m->buffer[ppos.x][ppos.y] = (RayTile){0,0}; break;
                case SDLK_F1: mapgen(m); break;
                case SDLK_F2:
                    fog = !fog;
                    RF_SetFogEnabled(fog);
                    break;
                case SDLK_F3:
                    limit30 = !limit30;
                    RF_SetFramerate(30 + (limit30 * 30));
                    break;

                case SDLK_F5:
                    selBlock = pmod(selBlock - 1, 48);
                    break;
                case SDLK_F6:
                    selBlock = pmod(selBlock + 1, 48);
                    break;

                case SDLK_F7:
                    RF_SaveMapFile("/tmp/lightrcg.test.map");
                    break;
                case SDLK_F8:
                    RF_LoadMapFile("/tmp/lightrcg.test.map");
                    m = r->map;
                    break;

                case SDLK_ESCAPE: RF_StopRunning(); break;
                }
        }
        // RF_ClearFramebuffer(0, 0, 0);
        RF_ShowTextureByID(sky, P{0, 0});
        RF_UpdateHorizon();
        RF_UpdateWalls();
        RF_UpdateSprites();
        RF_UpdatePlayer(1);

        RF_ShowTextureByID(selBlock, P{GAME_W - 16, 0});
        RF_ShowTextureByID(skeletonTex, P{GAME_W - 60, 60});
        printInformation(r, font);

        RF_Update();
        // RF_UpdateAll(1);
    }

    R_FreeFont(font);
    RF_End();
    return 0;
}
