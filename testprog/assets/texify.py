#!/usr/bin/env python3
# Actual "texify" script that can be used anywhere
# packs all the given textures in one file
from PIL import Image, ImageSequence
import sys

def print_usage():
    print("""
lightrcg .tex file generator
USAGE: texify [-h] [-o FILE] [-s WIDTHxHEIGHT] FILES...
  -s single-file mode, useful for font bitmaps (all characters in one image)
     width and height must be specified
  -g gif mode, takes a gif and turn it into a bitmap
  -o output filename
  -h get this help message
""".strip())

def die(message):
    print(message, file=sys.stderr)
    sys.exit(1)

def multiple_mode_buffering(files):
    buffers = []
    for filename in files:
        with Image.open(filename) as im:
            im = im.convert('RGBA')
            buffer = []
            for y in range(im.height):
                for x in range(im.width):
                    buffer += im.getpixel((x, y))
            buffers.append(((im.width, im.height), bytes(buffer)))
    return buffers

def single_mode_buffering(filename, w, h):
    buffers = []
    with Image.open(filename) as im:
        im = im.convert('RGBA')
        for oy in range(0, im.height, h):
            for ox in range(0, im.width, w):
                buffer = []
                for y in range(h):
                    for x in range(w):
                        buffer += im.getpixel((ox + x, oy + y))
                buffers.append(((w, h), bytes(buffer)))
    return buffers

def gif_mode_buffering(filename):
    buffers = []
    with Image.open(filename) as im:
        for frame in ImageSequence.Iterator(im):
            buffer = []
            frame = frame.convert('RGBA')
            for y in range(0, frame.height):
                for x in range(0, frame.width):
                    buffer += frame.getpixel((x, y))
            buffers.append(((frame.width, frame.height), bytes(buffer)))
    return buffers

int2bytes = lambda n: bytes([(n & 0xff00) >> 8, n & 0xff])

def write_to_tex(buffers, output):
    print("Packing", len(buffers), "bitmaps in", output, file=sys.stderr)
    sz = len(buffers)
    if sz > 0xffff: die("Maximum .tex array size exceeded")
    
    with open(output, "wb") as f:
        f.write(b'LRBMP\x01') # Writing magic number
        f.write(int2bytes(sz)) # Amount of bitmaps
        for buffer in buffers:
            (width, height), buf = buffer
            f.write(int2bytes(width) + int2bytes(height))
            f.write(buf)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print_usage()
        sys.exit(1)
    
    mode = 'multiple'    # Multiple or single file mode?
    output = 'out.tex'   # Output file name?
    files = []           # Files to pass?
    dims = (0, 0)        # Dimensions in single file mode?
    args = sys.argv[1:]  # Arguments
    i = 0
    # Time for a lazily written Python script.
    # Python is pretty damn good for lazy ass scripts like this ngl.

    # Arguments parsing: the lazy way
    while i < len(args):
        arg = args[i]
        if arg.startswith('-'):
            arg = args[i][1:]
            if arg == 'h':
                print_usage()
                sys.exit(0) # No need to go further
            elif arg == 'g':
                mode = 'gif'
            elif arg == 's':
                i += 1
                mode = 'single'

                try:
                    dims = tuple(int(x) for x in args[i].split('x'))
                except ValueError:
                    die("Invalid dimension format")
                if len(dims) != 2: die("Invalid dimension format")
            elif arg == 'o':
                i += 1
                output = args[i].strip()
        else:
            files.append(arg.strip())
        i += 1

    if len(files) == 0:
        die("No file specified")
    elif len(files) != 1 and mode in ['single', 'gif']:
        die("You cannot pass multiple files in single mode")

    # Time to parse the files
    if mode == 'multiple':
        buffers = multiple_mode_buffering(files)
    elif mode == 'gif':
        buffers = gif_mode_buffering(files[0])
    else:
        buffers = single_mode_buffering(files[0], *dims)
    
    write_to_tex(buffers, output)
