# lightrcg

![](doc/screenshot.png)

lightrcg is a lightweight pseudo-3D game engine written in C. It is
mostly focused around raycasting, a pseudo-3D rendering technique old
video games such as Ken's Labyrinth or Wolfenstein 3D used.

If you intend to use lightrcg with something else than SDL, I apologize
for the additional dependency. I am planning to make the frontend
optional as lightrcg does not need SDL to function (you can even do a
terminal raycasting game if you want to).

### Features

 * Basic raycasting (walls, textures, sprites)
 * Animated textures
 * Sprites with Z height (basically it can go up and down)
 * an SDL Front-end to make game programming easier (well, i try)
 * Custom floor and ceilings
 * Lightmap (basically you can set the luminosity of each map tile)
 * Custom bitmap file format (not really a feature, I just didn't bothered to use
   some library for PNG and JPG images)
 * Custom map file format
 * 100% software rendering, no GPU acceleration

### License

This work is licensed under the Mozilla Public License 2.0
